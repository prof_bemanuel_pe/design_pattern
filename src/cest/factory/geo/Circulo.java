package cest.factory.geo;

public class Circulo implements Forma {
	private int raio;
	
	public Circulo(int r) {
		this.raio = r;
	}
	
	public void desenha() {
		System.out.println("circulo");
	}
	
	public void area() {
		System.out.println("Area:" + (raio*raio));
	}
}

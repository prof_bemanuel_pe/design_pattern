package cest.factory.geo;

public class Cliente {
	public static void main(String[] args) {
		Fabrica fab = new Fabrica();
		Forma circ = fab.criaForma(Fabrica.Tipo.CIRCULO);
		Forma quad = fab.criaForma(Fabrica.Tipo.QUADRADO);
		Forma ret = fab.criaForma(Fabrica.Tipo.RETANGULO);
		
		circ.desenha();
		quad.desenha();
		
		circ.area();
		quad.area();
		
		ret.desenha();
		ret.area();
	}
}

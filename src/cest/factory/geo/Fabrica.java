package cest.factory.geo;

import java.util.Random;

public class Fabrica {
	private Random rand = new Random();

	public enum Tipo {
		CIRCULO, QUADRADO, RETANGULO;
	}

	public Forma criaForma(Tipo t) {
		Forma resultado = null;
		switch(t) {
			case CIRCULO: 
				resultado = new Circulo(rand.nextInt());
				break;
			case QUADRADO:
				resultado = new Quadrado(rand.nextInt());
				break;
			case RETANGULO:
				resultado = new Quadrado(rand.nextInt(), rand.nextInt());
		}
		/*
		if (t == Tipo.CIRCULO) {
			resultado = new Circulo(rand.nextInt());
		}
		if (t == Tipo.QUADRADO) {
			resultado = new Quadrado(rand.nextInt());
		}
		if (t == Tipo.RETANGULO) {
			resultado = new Quadrado(rand.nextInt(), rand.nextInt());
		}*/
		return resultado;
	}

}

package cest.factory.geo;

public class Quadrado implements Forma {
	private int base;
	private int altura;
	
	public Quadrado(int b, int h) {
		this.base = b;
		this.altura = h;
	}
	
	public Quadrado(int b) {
		this.base = b;
		this.altura = b;
	}

	public void desenha() {
		if (base==altura) {
		 System.out.println("quadrado");
		} else {
			System.out.println("retangulo");
		}
	}
	
	public void area() {
		System.out.println("Area:" + (base*altura));
	}
	
}

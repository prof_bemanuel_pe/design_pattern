package cest.singleton;
public class MeuContador {
	private int i = 0; //visivel para a instancia
	private static MeuContador mc = new MeuContador(); //carrega na memoria
	
	private MeuContador() {}	
	public static MeuContador getInstance() { //carrega na memoria
		return mc;
	}
	
	public int proximo() { //visivel para a instancia, publico
		return ++i;
	}
}

package cest.singleton;

public class TestaSingleton {

	public static void main(String[] args) {
		ClasseA ca = new ClasseA();
		ClasseB cb = new ClasseB();
		
		for (int i=1;i<10;i++) {
			cb.proximoNumero();
			ca.proximoNumero();
			cb.proximoNumero();
			cb.proximoNumero();
			ca.proximoNumero();
			ca.proximoNumero();
			
		}
	}
	
}
